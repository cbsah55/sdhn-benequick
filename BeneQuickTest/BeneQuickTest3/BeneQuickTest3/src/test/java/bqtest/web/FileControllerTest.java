/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bqtest.web;

import bqtest.service.MemberImporter;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;





/**
 *
 * @author Chaitanya>
 */
@RunWith(SpringRunner.class)
@WebMvcTest(controllers=FileController.class)
public class FileControllerTest {
    
    
    @Autowired
    private MockMvc mvc;
    
    
    @MockBean
    private  MemberImporter memberImporterService;
   
    @Test
    public void testLoadData()throws Exception {
      
       mvc.perform(get("/api/load-data")
      .contentType("application/json"))     
      .andExpect(status().isOk());  
                
    }
    
     @Test
    public void testLoadDataNotFound()throws Exception {
      
       mvc.perform(get("/api")
      .contentType("application/json"))     
      .andExpect(status().isNotFound());  
                
    }
    
}
