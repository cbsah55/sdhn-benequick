/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bqtest.service;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Chaitanya>
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties={
    "config.file-to-load = ${user.dir}/Members.txt",
})
public class MemberImporterImplTest {
    
   @Value("${config.file-to-load}")
   String filePath;
   
   @Autowired
   private MemberImporter memberImporterService;
   
   private static final Logger log = LoggerFactory.getLogger(MemberImporterImplTest.class);
    /**
     * Test of processFile method, of class MemberImporterImpl.
     */
    @Test
    public void testProcessFile() throws Exception {
        log.info("file path {}",filePath);
        File uploadedFile = new File(filePath);
        //get list of members from file
        List<Member> memberList = new ArrayList<>();
                 memberList =Files.lines(uploadedFile.toPath())
                .map(line-> {
                  List<String> output = new ArrayList<>();
                    final String regex = "^(.{12})(.{25})(.{25})(.{30})(.{20})(.{4})(.{5}).*";
                    final Pattern pattern = Pattern.compile(regex);
                    final Matcher matcher = pattern.matcher(line);
                    if (matcher.matches()) {
     for (int i = 1; i <= matcher.groupCount(); i++) {
         output.add(matcher.group(i));
     }
  }
                    return output.toArray(new String[0]);
                        })
                .map(arr -> {
                    Member member = new Member();
                    member.setId(arr[0]);
                    member.setLastName(arr[1]);
                    member.setFirstName(arr[2]);
                    member.setAddress(arr[3]);
                    member.setCity(arr[4]);
                    member.setState(arr[5]);
                    member.setZip(arr[6]);
                    return member;
                })
                .collect(Collectors.toList());
                 List<Member> list = new ArrayList<>(memberList);
                 
         //data from service MembersImporterImpl
        Map<String,List<Member>> membersFromService = memberImporterService.processFile(uploadedFile);
        
        //splitting list of members according to states
        Map<String,List<Member>> membersByState = new HashMap<>();
        Set<String> stateList = memberList.stream().map(Member::getState).collect(Collectors.toSet());
        stateList.forEach(s -> membersByState.put(s,list.stream().filter(member ->
                member.getState().equals(s)).collect(Collectors.toList())));
        
        assertEquals(membersByState.containsKey("MN"),membersFromService.containsKey("MN"));
        assertEquals(membersByState.size(),membersFromService.size());
        
       
    }

    
}
