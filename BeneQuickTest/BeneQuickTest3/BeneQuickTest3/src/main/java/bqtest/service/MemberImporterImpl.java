package bqtest.service;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class MemberImporterImpl implements MemberImporter {


    public Map<String, List<Member>> processFile(File inputMemberFile) throws IOException {

        List<Member> membersFromFile = importMembers(inputMemberFile);

        return splitMembersByState(membersFromFile);

    }


    private List<Member> importMembers(File inputFile) throws IOException {
        return Files.lines(inputFile.toPath())
                .map(line-> {
                  List<String> output = new ArrayList<>();
                    final String regex = "^(.{12})(.{25})(.{25})(.{30})(.{20})(.{4})(.{5}).*";
                    final Pattern pattern = Pattern.compile(regex);
                    final Matcher matcher = pattern.matcher(line);
                    if (matcher.matches()) {
     for (int i = 1; i <= matcher.groupCount(); i++) {
         output.add(matcher.group(i));
     }
  }
                    return output.toArray(new String[0]);
                        })
                .map(this::toMember)
                .collect(Collectors.toList());

    }
    private Member toMember (String[] data){
        Member member = new Member();
        member.setId(data[0]);
        member.setLastName(data[1]);
        member.setFirstName(data[2]);
        member.setAddress(data[3]);
        member.setCity(data[4]);
        member.setState(data[5]);
        member.setZip(data[6]);
        return member;
    }

    private Map<String, List<Member>> splitMembersByState(List<Member> validMembers) {
        Map<String,List<Member>> membersByState = new HashMap<>();
        Set<String> stateList = validMembers.stream().map(Member::getState).collect(Collectors.toSet());
        stateList.forEach(s -> membersByState.put(s,validMembers.stream().filter(member ->
                member.getState().equals(s)).collect(Collectors.toList())));
        return membersByState;

    }


}
