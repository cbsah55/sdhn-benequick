angular.module('bqTestModule', [])
    .controller('FetchController', ['$scope', '$http',
        function ($scope, $http) {
                $scope.members = [];
                $scope.dataList=[];
                $scope.stateList = [];
            let states = [];
            fetchData();

            function fetchData(){
                $http({method: 'GET', url: 'api/load-data'}).then(function successCallback(response) {
                    console.log(response.data);
                    $scope.members = response.data;
                    states=Object.keys($scope.members);
                    $scope.stateList = states
                    console.log($scope.stateList)

                }, function errorCallback(reason) {
                    console.log('error ' + reason)
                });
            }
           $scope.searchMembers =function (stateName){

               $scope.dataList = $scope.members[stateName]
                console.log($scope.dataList)

            }

        }]);
